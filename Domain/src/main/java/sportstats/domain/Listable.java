package sportstats.domain;

import java.io.Serializable;

/**
 *
 * @author thomas
 */
public interface Listable extends Serializable {

    Long getId();

    String getName();
}
