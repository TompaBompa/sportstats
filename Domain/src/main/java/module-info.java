module cleaning.domain {
    requires org.hibernate.orm.core;
    requires java.persistence;
    requires com.fasterxml.jackson.annotation;
    
    exports sportstats.domain;
}
