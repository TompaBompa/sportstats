package sportstats.service;

/**
 *
 * @author thomas
 */
public class ServiceException extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }
}
