package sportstats.service;

import sportstats.domain.Sport;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sportstats.domain.*;
import sportstats.repository.SportRepository;
import sportstats.service.util.local.ListableProxy;

/**
 *
 * @author thomas
 */
@Service
public class SportService {

    private final SportRepository repository;

    @Autowired
    public SportService(SportRepository repository) {
        this.repository = repository;
    }

    public Sport saveSport(Sport sport) {
        return repository.save(sport);
    }

    public Sport getSport(Long id) {
        return repository.getById(id);
    }

    public List<Listable> getAllSports() {
        return ListableProxy.listOf(repository.findAll());
    }

    public List<Listable> findByName(String name) {
        return ListableProxy.listOf(repository.findByName(name));
    }
}
