package sportstats.service.util.local;

import sportstats.domain.Listable;
import java.util.List;

/**
 *
 * @author thomas
 */
public class ListableProxy implements Listable {

    private final Listable delegate;

    public ListableProxy(Listable delegate) {
        this.delegate = delegate;
    }

    @Override
    public Long getId() {
        return delegate.getId();
    }

    @Override
    public String getName() {
        return delegate.getName();
    }

    public static List<Listable> listOf(List<? extends Listable> source) {
        return source.stream()
                .map(p -> (Listable) new ListableProxy(p))
                .toList();
    }
}
