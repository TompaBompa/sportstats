module cleaning.service {
    exports sportstats.service;
    
    requires spring.context;
    requires cleaning.repository;
    requires transitive cleaning.domain;
}
