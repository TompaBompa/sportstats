package sportstats.repository;

import java.util.List;
import sportstats.domain.Sport;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface SportRepository extends JpaRepository<Sport, Long> {

    @Query("FROM Sport s WHERE name LIKE CONCAT(?1, '%')")
    List<Sport> findByName(String name);
}
