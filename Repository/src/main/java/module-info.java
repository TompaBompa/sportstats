module cleaning.repository {
    requires spring.context;
    requires spring.data.jpa;
    requires cleaning.domain;
    
    exports sportstats.repository;
}
