module sportstats.rest {
    requires cleaning.service;
    requires java.logging;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.web;
    requires spring.beans;
}
