package sportstats.rest.log;

import java.util.function.Supplier;

/**
 *
 * @author thomas
 */
public interface ResourceLogger {

    public enum Type {
        STANDARD, SIMPLE;
    }

    void log(Supplier<String> messageSupplier);

    void defaultLog(Object... params);

    static ResourceLogger of(Type type) {
        return type == Type.STANDARD
                ? new StandardResourceLogger()
                : new SimpleResourceLogger();
    }
}
