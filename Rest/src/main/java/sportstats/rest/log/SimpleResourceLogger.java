package sportstats.rest.log;

import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.*;

/**
 *
 * @author thomas
 */
public class SimpleResourceLogger implements ResourceLogger {

    @Override
    public void log(Supplier<String> messageSupplier) {
        System.out.println(messageSupplier);
    }

    @Override
    public void defaultLog(Object... params) {
        log(() -> Thread.currentThread().getStackTrace()[7].getMethodName()
                + "(" + Stream.of(params).map(o -> Objects.toString(o))
                        .collect(Collectors.joining(", ")) + ")");
    }
}
