package sportstats.rest.log;

import java.util.Objects;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.stream.*;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class StandardResourceLogger implements ResourceLogger {

    @Override
    public void log(Supplier<String> messageSupplier) {
        Logger.getGlobal().info(messageSupplier);
    }

    @Override
    public void defaultLog(Object... params) {
        log(() -> Thread.currentThread().getStackTrace()[7].getMethodName()
                + "(" + Stream.of(params).map(o -> Objects.toString(o))
                        .collect(Collectors.joining(", ")) + ")");
    }
}
