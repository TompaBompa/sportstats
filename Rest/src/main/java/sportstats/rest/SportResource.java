package sportstats.rest;

import sportstats.service.SportService;
import sportstats.domain.Sport;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sportstats.domain.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("sport")
public class SportResource {

    private final SportService service;

    @Autowired
    public SportResource(SportService service) {
        this.service = service;
    }

    @PostMapping()
    public Sport saveSport(@RequestBody Sport sport) {
        return service.saveSport(sport);
    }
    
    @GetMapping("{id}")
    public Sport getSportById(@PathVariable Long id) {
        return service.getSport(id);
    }
    
    @GetMapping
    public List<Listable> getAllSports() {
        return service.getAllSports();
    }
    
    @GetMapping("find/{name}")
    public List<Listable> getAllSports(@PathVariable String name) {
        return service.findByName(name);
    }
}
