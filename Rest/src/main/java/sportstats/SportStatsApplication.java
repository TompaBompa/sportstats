package sportstats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportStatsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SportStatsApplication.class, args);
    }
}
